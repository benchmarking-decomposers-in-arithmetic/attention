from __future__ import annotations
import os
from pathlib import Path
import time
from typing import Tuple, Optional
from numpy import ndarray
import tensorflow as tf

Dataset = tf.data.Dataset
logical_not = tf.math.logical_not
equal = tf.math.equal
from tensorflow.keras.backend import clear_session
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.preprocessing.sequence import pad_sequences

Checkpoint = tf.train.Checkpoint
CheckpointManager = tf.train.CheckpointManager
latest_checkpoint = tf.train.latest_checkpoint
import numpy as np

ndarray = np.ndarray
from tqdm import tqdm

# from tensorflow.keras.train import Checkpoint
from model_defs import Encoder, Decoder


class Train:
    def __init__(
        self,
        data: Data,
        epochs: int,
        encoder: Encoder,
        decoder: Decoder,
        optimizer: ABCMeta,
        load_saved: bool = False
    ):
        # clear_session()
        self.data = data
        self.epochs = epochs
        self.encoder = encoder
        self.decoder = decoder
        self.optimizer = optimizer

        self.dataset = data.dataset
        try:
            self.batch_size = data.dataset._batch_size.numpy()
        except AttributeError:
            raise AttributeError("Dataset must be batched!")

        self.steps_per_epoch = data.questions_tensor.shape[0] // self.batch_size

        self.load_saved = load_saved
        # we really want this to be `trained_models/CHECKPOINTS-{self.data.datafilename...}` but it's not working.
        self.checkpoint_dir = Path(f"CHECKPOINTS-{self.data.datafilename[:-4]}")
        self.checkpoint_dir.mkdir(exist_ok=True, parents=True)
        self.checkpoint_prefix = self.checkpoint_dir / "ckpt"
        self.checkpoint = Checkpoint(
            optimizer=self.optimizer, encoder=self.encoder, decoder=self.decoder
        )
        self.manager = CheckpointManager(
            self.checkpoint, directory=self.checkpoint_dir, max_to_keep=4
        )

        if load_saved:
            restoration = self.checkpoint.restore(
                latest_checkpoint(self.checkpoint_dir)
            )


    @staticmethod
    def loss_function(actual, predicted) -> float:

        loss_object = SparseCategoricalCrossentropy(from_logits=True, reduction="none")
        mask = logical_not(equal(actual, 0))
        loss_ = loss_object(actual, predicted)

        mask = tf.cast(mask, dtype=loss_.dtype)
        loss_ *= mask

        return tf.reduce_mean(loss_)

    @tf.function
    def train_step(
        self, inp: EagerTensor, targ: EagerTensor, enc_hidden: Tensor
    ) -> float:
        """Mutates the state of optimizer, encoder.trainable_variables, and decoder.trainable_variables"""
        loss = 0

        with tf.GradientTape() as tape:
            enc_output, enc_hidden = self.encoder(inp, enc_hidden)

            dec_hidden = enc_hidden

            dec_input = tf.expand_dims(
                [self.data.answers_tokenizer.word_index["("]] * self.batch_size, 1
            )

            # teacher forcing -- feeding the target as the next input
            for t in range(1, targ.shape[1]):
                # passing enc_output to the decoder
                predictions, dec_hidden, _ = self.decoder(
                    dec_input, dec_hidden, enc_output
                )

                loss += self.loss_function(targ[:, t], predictions)

                # using teacher forcing
                dec_input = tf.expand_dims(targ[:, t], 1)

            batch_loss = loss / int(targ.shape[1])

            variables = (
                self.encoder.trainable_variables + self.decoder.trainable_variables
            )

            gradients = tape.gradient(loss, variables)

            self.optimizer.apply_gradients(zip(gradients, variables))

            return batch_loss

    def predict(self, question_text: ndarray, load_saved: bool = False) -> str:
        """predict an answer from a question"""
        if load_saved:
            restoration = self.checkpoint.restore(
                latest_checkpoint(self.checkpoint_dir)
            )

        result = str()

        inputs = tf.convert_to_tensor(
            pad_sequences(
                [
                    [
                        self.data.questions_tokenizer.word_index[char]
                        for char in question_text
                    ]
                ],
                maxlen=self.data.questions_tensor.shape[1],
                padding="post",
            )
        )

        hidden = [tf.zeros((1, self.encoder.enc_units))]

        enc_out, enc_hidden = self.encoder(inputs, hidden)

        dec_hidden = enc_hidden

        dec_input = tf.expand_dims([self.data.answers_tokenizer.word_index["("]], 0)
        for t in range(self.data.answers_tensor.shape[1]):
            predictions, dec_hidden, attention_weights = self.decoder(
                dec_input, dec_hidden, enc_out
            )

            attention_weights = tf.reshape(attention_weights, (-1,))

            predicted_id = tf.argmax(predictions[0]).numpy()

            next_char = self.data.answers_tokenizer.index_word[predicted_id]

            if next_char == ")":
                return result
            result += next_char

            dec_input = tf.expand_dims([predicted_id], 0)
        return result

    # @tf.function
    def accuracy(self, val_data_texts: Optional[Tuple[ndarray, ndarray]] = None) -> float:
        """Get an %age accuracy score with the validation data."""

        def strip_startend_toks(answer: str) -> str:
            return answer[1:-1]

        if val_data_texts:
            val_texts = zip(val_data_texts[0], val_data_texts[1])
            val_N = val_data_texts[0].shape[0]
        else:
            val_texts = zip(self.data.questions_val_text, self.data.answers_val_text)
            val_N = self.data.questions_val_text.shape[0]
        
        return (
            sum(
                self.predict(question) == strip_startend_toks(answer)
                for question, answer in tqdm(val_texts)
            )
            / val_N
        )

    def train_loop(
        self, restore_checkpoint: bool = True, save_checkpoints: bool = True
    ):
        # clear_session()
        if restore_checkpoint:
            restoration = self.checkpoint.restore(
                latest_checkpoint(self.checkpoint_dir)
            )

        for epoch in range(self.epochs):
            start = time.time()

            enc_hidden = self.encoder.initialize_hidden_state()
            total_loss = 0

            for (batch, (inp, targ)) in enumerate(
                self.dataset.take(self.steps_per_epoch)
            ):
                batch_loss = self.train_step(inp, targ, enc_hidden)
                total_loss += batch_loss

                if batch % 100 == 0:  # report the occasional batch
                    print(
                        f"Epoch {epoch + 1} Batch {batch} Loss {total_loss / self.steps_per_epoch:.5}"
                    )

            if save_checkpoints and (epoch + 1) % 2 == 0:  # save every other epoch
                self.manager.save()

            real_loss = total_loss / self.steps_per_epoch
            print(f"Epoch {epoch + 1} Loss {real_loss}")
            # if real_loss < 0.0001:
            #    break

            print(
                f"Time taken for one epoch: {time.time() - start:.4} seconds", end="\n"
            )

            if (epoch + 1) % 5 == 0:  # report accuracy every 5th epoch
                accuracy = self.accuracy()
                print(f"\tCurrent validation accuracy: {accuracy}")
                if accuracy > 0.9:
                    return real_loss, accuracy

        accuracy = self.accuracy()
        print(f"\tCurrent validation accuracy: {accuracy} \n")
        return real_loss, accuracy
