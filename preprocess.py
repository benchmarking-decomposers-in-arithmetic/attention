import io
import os
from typing import List, Tuple, Iterator, Union
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

# from tensorflow.data import Dataset
import tensorflow as tf

Dataset = tf.data.Dataset
from numpy import ndarray, array
from utils import iterator_of_tuples_to_tuple_of_iterators


class Preprocess:
    def __init__(self, datafilename: str, sep_token: str) -> None:
        self.datafilename = datafilename
        self.sep_token = sep_token
        self.questions, self.answers = iterator_of_tuples_to_tuple_of_iterators(
            self._clean_strings(self._separate(self._load()))
        )

    def _load(self) -> List[str]:
        """Loads in data at a filepath"""
        base_path = os.path.dirname(os.path.abspath(__file__))
        filepath = os.path.join(base_path, "data", self.datafilename)

        with io.open(filepath, "r") as infile:
            return infile.readlines()

    def _separate(self, observations: List[str]) -> Iterator[Tuple[str, str]]:
        """Assuming sep_token appears once per string, return iterator of twoples."""
        return (
            tuple(observation.split(self.sep_token)) for observation in observations
        )

    def _clean_strings(
        self, questions_answered: Iterator[Tuple[str, str]]
    ) -> Iterator[Tuple[str, str]]:
        """Operate on each question-answer pair.

        1. prepend <START> and append <END> to each question and each answer.
        2. lowercase
        """
        start = "("
        end = ")"
        return (
            (
                start + question.strip().lower() + end,
                start + answer.strip().lower() + end,
            )
            for question, answer in questions_answered
        )


class Data(Preprocess):
    def __init__(
        self,
        datafilename: str,
        sep_token: str,
        shuffle_buffer_size: int,
        batch_size: int,
        train_size: Union[int, float],
    ) -> None:
        """Data."""
        super(Data, self).__init__(datafilename, sep_token)
        self.shuffle_buffer_size = shuffle_buffer_size
        self.batch_size = batch_size
        self.train_size = train_size
        self.questions = array(list(self.questions))
        self.answers = array(list(self.answers))
        (
            self.questions_train_text,
            self.answers_train_text,
            self.questions_val_text,
            self.answers_val_text,
        ) = self._train_test_split_text()
        self.questions_tensor, self.questions_tokenizer = self._tokenize(self.questions)
        self.answers_tensor, self.answers_tokenizer = self._tokenize(self.answers)
        (
            self.questions_train,
            self.answers_train,
            self.questions_val,
            self.answers_val,
        ) = self._train_test_split_tensor()
        self.dataset = (
            Dataset.from_tensor_slices((self.questions_train, self.answers_train))
            .shuffle(self.shuffle_buffer_size)
            .batch(self.batch_size, drop_remainder=True)
        )
        self.validation_dataset = Dataset.from_tensor_slices(
            (self.questions_val, self.answers_val)
        ).batch(self.batch_size, drop_remainder=True)

    def _tokenize(self, texts: ndarray) -> Tuple[ndarray, Tokenizer]:
        tokenizer = Tokenizer(char_level=True)
        tokenizer.fit_on_texts(texts)

        tensor = pad_sequences(tokenizer.texts_to_sequences(texts), padding="post")
        return tensor, tokenizer

    def _train_test_split_tensor(self) -> Tuple[ndarray, ndarray, ndarray, ndarray]:
        if isinstance(self.train_size, float):
            train_size = int(self.train_size * self.questions.shape[0])
        else:
            train_size = self.train_size
        return (
            self.questions_tensor[:train_size],
            self.answers_tensor[:train_size],
            self.questions_tensor[train_size:],
            self.answers_tensor[train_size:],
        )

    def _train_test_split_text(self) -> Tuple[ndarray, ndarray, ndarray, ndarray]:
        if isinstance(self.train_size, float):
            train_size = int(self.train_size * self.questions.shape[0])
        else:
            train_size = self.train_size
        return (
            self.questions[:train_size],
            self.answers[:train_size],
            self.questions[train_size:],
            self.answers[train_size:],
        )
