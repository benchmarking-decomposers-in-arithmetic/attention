from typing import Iterator, Tuple, TypeVar

A = TypeVar("A")


def iterator_of_tuples_to_tuple_of_iterators(
    inp: Iterator[Tuple[A, A]]
) -> Tuple[Iterator[A], Iterator[A]]:
    """Iterator of tuples to tuple of iterators.

    This casts to list, which isn't optimal for memory.
    What is needed is some theory of copying generators.
    At a glance, copy.copy or itertools.tee didn't work for various reasons.
    """
    inplist = list(inp)
    return ((tup[0] for tup in inplist), (tup[1] for tup in inplist))
