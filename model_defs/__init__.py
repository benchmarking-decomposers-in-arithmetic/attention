"""Custom Model and Layer Definitions."""
import tensorflow as tf

tanh = tf.nn.tanh
softmax = tf.nn.softmax
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Layer, Embedding, GRU, Dense


class Encoder(Model):
    def __init__(self, vocab_size, embedding_dim, enc_units, batch_size):
        super(Encoder, self).__init__()
        self.batch_size = batch_size
        self.enc_units = enc_units
        self.embedding = Embedding(vocab_size, embedding_dim)
        self.gru = GRU(
            enc_units,
            return_sequences=True,
            return_state=True,
            recurrent_initializer="glorot_uniform",
        )

    def call(self, x, hidden):
        x = self.embedding(x)
        output, state = self.gru(x, initial_state=hidden)
        return output, state

    def initialize_hidden_state(self):
        return tf.zeros((self.batch_size, self.enc_units))


class BahdanauAttention(Layer):
    def __init__(self, units):
        super(BahdanauAttention, self).__init__()
        self.W1 = Dense(units)
        self.W2 = Dense(units)
        self.V = Dense(1)

    def call(self, query, values):
        """
        Score hidden state shape == (batch_size, hidden_size)
        query_with_time_axis shape == (batch_size, 1, hidden_size)
        values shape == (batch_size, max_len, hidden_size)
        """
        # to broadcast addition along a time axis to calculate score
        query_with_time_axis = tf.expand_dims(query, 1)

        # score shape == (batch_size, max_length, 1)
        # we get 1 at the last axis because we are applying score to self.V
        # the shape of the tensor before apply self.V is (batch_size, max_length, units)
        attention_weights = softmax(
            self.V(tanh(self.W1(query_with_time_axis) + self.W2(values))), axis=1
        )

        context_vector = tf.reduce_sum(attention_weights * values, axis=1)

        return context_vector, attention_weights


class Decoder(Model):
    def __init__(self, vocab_size, embedding_dim, dec_units, batch_size):
        super(Decoder, self).__init__()
        self.batch_size = batch_size
        self.dec_units = dec_units
        self.embedding = Embedding(vocab_size, embedding_dim)
        self.gru = GRU(
            self.dec_units,
            return_sequences=True,
            return_state=True,
            recurrent_initializer="glorot_uniform",
        )
        self.fc = Dense(vocab_size)

        # used for attention
        self.attention = BahdanauAttention(self.dec_units)

    def call(self, x, hidden, enc_output):
        """ """
        # output shape == (batch_size, max_length, hidden_size)
        context_vector, attention_weights = self.attention(hidden, enc_output)

        # x shape after passing through embedding == (batch_size, 1, embedding_dim)
        # x shape after concatenation == (batch_dize, 1, embedding_dim + hidden_size)
        x = tf.concat([tf.expand_dims(context_vector, 1), self.embedding(x)], axis=-1)
        # pass concatenated vector to gru
        output, state = self.gru(x)

        # output shape == (batch_size * 1, hidden_size)
        output = tf.reshape(output, (-1, output.shape[2]))

        # output shape == (batch_size, vocab)
        return self.fc(output), state, attention_weights
