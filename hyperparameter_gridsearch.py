#!/usr/bin/env python
from __future__ import annotations
import logging
from typing import List, Dict
from pathlib import Path
from configparser import ConfigParser

# import tensorflow as tf
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend
from data.create_data import Retrieve
from preprocess import Data
from model_defs import Encoder, Decoder
from train_loop_utils import Train

config = ConfigParser()
config.read("dataparameters.ini")


BASE_URL = config["BASICS"]["BASE_URL"]
ENDPOINT = config["BASICS"]["ENDPOINT"]
EPOCHS = config["BASICS"].getint("EPOCHS")
PARAMS = config["API_PARAMS"]

BUFFER_SIZE = config["DATASET_PARAMS"].getint("BUFFER_SIZE")
BATCH_SIZE = config["DATASET_PARAMS"].getint("BATCH_SIZE")
TRAIN_TEST_SPLIT = config["DATASET_PARAMS"].getfloat("TRAIN_TEST_SPLIT")

hparams = {
    "encoder__embedding_dim": [1024 - 256, 1024],
    "decoder__embedding_dim": [1024 - 256, 1024],
    "units": [1024],
}


logging.basicConfig(filename="hp_tuning.log", level=logging.INFO)


def my_argmin(
    losses: List[float],
    accuracies: List[float],
    configurations: Dict[str, int],
    retrieve: Retrieve,
    logging,
) -> None:
    """Writes to logfile."""
    argmin_loss, (loss, accuracy) = sorted(
        enumerate(zip(losses, accuracies)),
        key=lambda t: (round(t[1][0].numpy(), 4), -t[1][1]),
    )[0]

    logging.info(f"ON DATA: {retrieve.filename}")
    logging.info(
        f"lowest loss: {loss}, accuracy at that loss: {accuracy},\t"
        f"configuration: {configurations[argmin_loss]}"
    )
    logging.info(str())
    return None


if __name__ == "__main__":

    retrieve = Retrieve(BASE_URL, ENDPOINT, PARAMS)
    data = Data(
        retrieve.filename, PARAMS["sep"], BUFFER_SIZE, BATCH_SIZE, TRAIN_TEST_SPLIT
    )
    vocab_input_size = len(data.questions_tokenizer.word_index.keys()) + 1
    vocab_target_size = len(data.answers_tokenizer.word_index.keys()) + 1

    losses = list()
    accuracies = list()
    configurations = list()
    # for embedding_dim in hparams["embedding_dim"]:

    for units in hparams["units"]:
        for enc_emb_dim in hparams["encoder__embedding_dim"]:
            for dec_emb_dim in hparams["decoder__embedding_dim"]:
                encoder = Encoder(vocab_input_size, enc_emb_dim, units, BATCH_SIZE)
                decoder = Decoder(vocab_target_size, dec_emb_dim, units, BATCH_SIZE)
                adam = Adam()
                train = Train(data, EPOCHS, encoder, decoder, adam)
                print(
                    f"Units: {units}, "
                    f"encoder embedding dim: {enc_emb_dim}, "
                    f"decoder embedding dim: {dec_emb_dim}"
                )
                loss, accuracy = train.train_loop(
                    restore_checkpoint=False, save_checkpoints=False
                )
                losses.append(loss)
                accuracies.append(accuracy)
                configurations.append(
                    {
                        "encoder__embedding_dim": enc_emb_dim,
                        "decoder__embedding_dim": dec_emb_dim,
                        "units": units,
                    }
                )

    my_argmin(losses, accuracies, configurations, retrieve, logging)
