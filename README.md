A tutorial implementation of attentional seq2seq, via https://www.tensorflow.org/tutorials/text/nmt_with_attention

Written with TensorFlow 2.1. Also requires `requests` and `tqdm`. 

In `dataparameters.ini`, find the settings for the [simplemath](https://gitlab.com/benchmarking-decomposers-in-arithmetic/simplemath) API. The model checkpoint prefix is generated from the dataset filename, which is generated from given parameters. 

_Warning_, generated checkpoint name does not take into account hyperparameters. This can lead to the program crashing. 

If you're using the `Retrieve2` class in `data/create_data.py`, you're combining different API query params into one dataset (it's all preset for you). `Retrieve` is the class that uses `dataparameters.ini`. 
