#!/usr/bin/env python

from configparser import ConfigParser
from tensorflow.keras.optimizers import Adam
from data.create_data import Retrieve, Retrieve2, Retrieve3
from preprocess import Data
from model_defs import Encoder, Decoder
from train_loop_utils import Train

config = ConfigParser()
config.read("dataparameters.ini")

BASE_URL = config["BASICS"]["BASE_URL"]
ENDPOINT = config["BASICS"]["ENDPOINT"]
EPOCHS = config["BASICS"].getint("EPOCHS")
PARAMS = config["API_PARAMS"]

BUFFER_SIZE = config["DATASET_PARAMS"].getint("BUFFER_SIZE")
BATCH_SIZE = config["DATASET_PARAMS"].getint("BATCH_SIZE")
TRAIN_TEST_SPLIT = config["DATASET_PARAMS"].getfloat("TRAIN_TEST_SPLIT")

# retrieve = Retrieve(BASE_URL, ENDPOINT, PARAMS)
# retrieve = Retrieve2(BASE_URL, PARAMS["sep"])
retrieve = Retrieve3(BASE_URL, PARAMS["sep"])
data = Data(retrieve.filename, PARAMS["sep"], BUFFER_SIZE, BATCH_SIZE, TRAIN_TEST_SPLIT)

if __name__ == "__main__":

    hparams = ConfigParser()
    hparams.read("hyperparameters.ini")
    units = hparams["HYPERPARAMETERS"].getint("recurrent_units")
    enc_emb_dim = hparams["HYPERPARAMETERS"].getint("encoder__embedding_dim")
    dec_emb_dim = hparams["HYPERPARAMETERS"].getint("decoder__embedding_dim")
    vocab_input_size = len(data.questions_tokenizer.word_index.keys()) + 1
    vocab_target_size = len(data.answers_tokenizer.word_index.keys()) + 1

    encoder = Encoder(vocab_input_size, enc_emb_dim, units, BATCH_SIZE)
    decoder = Decoder(vocab_target_size, dec_emb_dim, units, BATCH_SIZE)
    adam = Adam()
    train = Train(data, EPOCHS, encoder, decoder, adam, load_saved=True)

    #   question_text = "(evaluate 9*3.)"  # 27
    #   question_text_2 = "(evaluate 9-5.)"  # 4
    #   print(train.predict(question_text, load_saved=True))
    #   print(train.predict(question_text_2, load_saved=True))

    train.train_loop()
    for high in (10, 100, 1000, 10000):
        val_retrieve = Retrieve(
            BASE_URL,
            "binaryarithmeticinequalitydata",
            {"num_observations": 1000, "sep": PARAMS["sep"], "high": high}
        )
        val_data = Data(
            val_retrieve.filename, PARAMS["sep"], BUFFER_SIZE, BATCH_SIZE, 0.0
        )

        accuracy = train.accuracy(
            val_data_texts=(val_data.questions_val_text, val_data.answers_val_text),
        )
        print(f"val on binaryarithmeticinequalitydata at high {high}: accuracy = {accuracy}")
