from typing import List, Dict, Any, Tuple
from functools import reduce
import os
import io
from json.decoder import JSONDecodeError
import requests
from random import sample


class Retrieve:
    def __init__(self, base_url: str, endpoint: str, params: Dict[str, Any]) -> None:
        self.base_url = base_url
        self.endpoint = endpoint
        self.params = params
        self.filename = self._make_filename(endpoint, params)
        self.base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # assume you're running it from top level, NOT from data subdir
        self.filepath = os.path.join(self.base_dir, "data", self.filename)
        if not os.path.isfile(self.filepath):
            print("Retrieving data from simplemath API")
            self.payload = self._get_payload()
            self._write_data()

    @staticmethod
    def _make_filename(endpoint: str, params: Dict[str, Any]) -> str:
        """Create the filename from params"""
        return (
            endpoint
            + "-"
            + "-".join(f"{key}={value}" for key, value in params.items())
            + ".txt"
        )

    def _get_payload(self) -> Dict[str, List[str]]:
        """Goes to an API endpoint and retrieves data."""

        payload = requests.get(
            f"{self.base_url}/{self.endpoint}?"
            + "&".join(f"{key}={value}" for key, value in self.params.items())
        )

        payload_json = payload.json()

        if "error" in payload_json.keys():
            # we know that in error scenarios output is a singleton list.
            error = payload_json["error"].pop()
            raise Exception(f"API returned an error response. {error}")
        return payload_json

    def _write_data(self):
        """Takes data and writes it to a file."""
        with io.open(self.filepath, "w") as out:
            out.write("\n".join(self.payload["observations"]))


class Retrieve2:
    def __init__(self, base_url: str, sep: str) -> None:
        self.base_url = base_url
        self.sep = sep
        self.endpoint = "binaryarithmeticdata"
        self.filename = f"{self.endpoint}-mixedlength.txt"
        self.base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        # assume you're running from toplevel, NOT from data subdir
        self.filepath = os.path.join(self.base_dir, "data", self.filename)
        if not os.path.isfile(self.filepath):
            print("Retrieving data from simplemath API")
            self.payload = self._get_payload()
            self._write_data()

    def _get_payload(self) -> Dict[str, List[str]]:
        parameters = (
            {"high": 10, "num_observations": 300, "sep": self.sep},
            {"high": 100, "num_observations": 30000, "sep": self.sep},
            {"high": 1000, "num_observations": 100000 - 30000 - 300, "sep": self.sep},
        )
        responses = [
            requests.get(
                f"{self.base_url}/{self.endpoint}?"
                + "&".join(f"{key}={value}" for key, value in params.items())
            )
            for params in parameters
        ]
        for response in responses:
            if "error" in response.json().keys():
                error = response.json()["error"].pop()
                raise Exception(f"API returned an error response. {error}")
        return {
            "observations": reduce(
                lambda x, y: x + y,
                (response.json()["observations"] for response in responses),
            )
        }

    def _write_data(self):
        """Takes data and writes it to a file."""
        with io.open(self.filepath, "w") as out:
            out.write("\n".join(self.payload["observations"]))


class Retrieve3:
    def __init__(self, base_url: str, sep: str) -> None:
        self.base_url = base_url
        self.sep = sep
        self.endpoints = ("binaryarithmeticdata", "inequalitydata")
        self.highs = (10, 100)
        self.num_observations = (10000, 90000, 10000, 40000)
        self.filename = f"binaryarithmetic-and-inequality-data-high=10-high=100"
        self.base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.filepath = os.path.join(self.base_dir, "data", self.filename)
        if not os.path.isfile(self.filepath):
            print("Retrieving data from simplemath API")
            self.payload = self._get_payload()
            self._write_data()

    def _get_payload(self) -> Dict[str, List[str]]:
        parameters = (
            (self.endpoints[0],
             {"high": self.highs[0], "num_observations": self.num_observations[0], "sep": self.sep}),
            (self.endpoints[0],
             {"high": self.highs[1], "num_observations": self.num_observations[1], "sep": self.sep}),
            (self.endpoints[1],
             {"high": self.highs[0], "num_observations": self.num_observations[2], "sep": self.sep}),
            (self.endpoints[1],
             {"high": self.highs[1], "num_observations": self.num_observations[3], "sep": self.sep})
        )
        responses = [
            requests.get(
                f"{self.base_url}/{endpoint}?"
                + "&".join(f"{key}={value}" for key, value in params.items())).json()
                for endpoint, params in parameters
        ]
        for response in responses:
            if "error" in response.keys():
                error = response["error"].pop()
                raise Exception(f"API returned an error response {error}")
        return {
            "observations": sample(reduce(
                lambda x, y: x + y,
                (response["observations"] for response in responses)
            ), sum(self.num_observations))
        }

    def _write_data(self):
        """Takes data and writes it to a file."""
        if not self.payload["observations"]:
            raise Exception("payload of observations is empty!")
        with io.open(self.filepath, "w") as out:
            out.write("\n".join(self.payload["observations"]))
